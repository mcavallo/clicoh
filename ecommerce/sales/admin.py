from django.contrib import admin
from django.db import models

from sales.models import OrderDetail, Order


class OrderDetailInline(admin.TabularInline):
    model = OrderDetail
    readonly_fields = ["price", "total_price"]
    extra = 1
    autocomplete_fields = ["product"]

    @admin.display()
    def total_price(self, obj):
        return (obj.price * obj.quantity) if obj.id else "-"


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "created",
        "modified",
        "total_products",
        "total_units",
        "total_price",
        "total_price_usd",
    ]
    inlines = [OrderDetailInline]
    readonly_fields = [
        "created",
        "modified",
        "total_products",
        "total_units",
        "total_price",
        "usd_exchange_value",
        "total_price_usd",
    ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate_totals()
        return qs

    @admin.display()
    def created(self, obj):
        return obj.created

    @admin.display()
    def modified(self, obj):
        return obj.modified

    @admin.display()
    def total_products(self, obj):
        return obj.total_products

    @admin.display()
    def total_units(self, obj):
        return obj.total_units

    @admin.display()
    def total_price(self, obj):
        return obj.total_price

    @admin.display()
    def total_price_usd(self, obj):
        return round(obj.total_price_usd, 2)
