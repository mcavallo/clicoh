from decimal import Decimal

from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Sum, F, Count, DecimalField, ExpressionWrapper
from django.db.models.functions import Coalesce
from django_extensions.db.models import TimeStampedModel
from django.utils.translation import gettext_lazy as _

from catalog.models import Product
from ecommerce.utils import get_exchange_rate


class OrderQuerySet(models.QuerySet):
    def annotate_totals(self):
        """Add totalizators to the orders within the same query.

        Annotates the fields:
            - total_products
            - total_units
            - total_price
            - total_price_usd

        """
        qs = self.annotate(
            total_products=Count("details"),
            total_units=Coalesce(Sum("details__quantity"), 0),
            total_price=Coalesce(
                Sum(
                    ExpressionWrapper(
                        F("details__price") * F("details__quantity"),
                        output_field=DecimalField(),
                    )
                ),
                Decimal("0"),
            ),
        ).annotate(
            total_price_usd=ExpressionWrapper(
                F("total_price") / F("usd_exchange_value"), output_field=DecimalField()
            ),
        )
        return qs

    @transaction.atomic
    def delete(self):
        """Delete order using also the details custom manager to return the stock."""
        orders_id = list(self.values_list("id", flat=True))
        OrderDetail.objects.filter(order_id__in=orders_id).delete()
        super().delete()


class OrderDetailQuerySet(models.QuerySet):
    @transaction.atomic
    def delete(self):
        """Delete the OrderDetails returning the stock"""
        qs = self.select_related("product")
        products_to_update = []
        for order_detail in qs:
            product = order_detail.product
            product.stock += order_detail.quantity
            products_to_update.append(product)
        Product.objects.bulk_update(products_to_update, ["stock"])
        super().delete()

    # TODO change of product
    @transaction.atomic
    def update(self, **kwargs):
        if "quantity" in kwargs.keys():
            qs = self.select_related("product")
            products_to_update = []

            # validate stock
            for detail in qs:
                detail.quantity = kwargs["quantity"]
                should_save_product, product = detail.validate_quantity()

                if should_save_product:
                    products_to_update.append(product)

            # update the products
            Product.objects.bulk_update(products_to_update, ["stock"])
        super().update(**kwargs)

    @transaction.atomic
    def bulk_create(self, objs, batch_size=None, ignore_conflicts=False):
        # Get the products involved in the transaction
        product_ids = [detail.product_id for detail in objs]
        products = {p.id: p for p in Product.objects.filter(id__in=product_ids)}

        # validate stock
        for detail in objs:
            should_save_product, product = detail.validate_quantity(
                products[detail.product_id]
            )

            if detail.price is None:
                detail.price = product.price

        # update the products and save the details
        Product.objects.bulk_update(products.values(), ["stock"])
        super().bulk_create(objs, batch_size, ignore_conflicts)

    # TODO change_product
    @transaction.atomic
    def bulk_update(self, objs, fields, batch_size=None):
        if "quantity" in fields:
            product_ids = [detail.product_id for detail in objs]
            products = {p.id: p for p in Product.objects.filter(id__in=product_ids)}
            products_to_update = []

            # validate stock
            for detail in objs:
                should_save_product, product = detail.validate_quantity(
                    products[detail.product_id]
                )

                if should_save_product:
                    products_to_update.append(product)

            # update the products
            Product.objects.bulk_update(products_to_update, ["stock"])
        super().bulk_update(objs, fields, batch_size)


class Order(TimeStampedModel):
    usd_exchange_value = models.DecimalField(
        verbose_name=_("usd exchange value"),
        max_digits=10,
        decimal_places=2,
        default=get_exchange_rate,
    )

    objects = OrderQuerySet.as_manager()

    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")

    def __str__(self):
        return f"{self.__class__.__name__} Nº {self.id}"

    def get_total(self):
        return self.details.aggregate(
            total=Sum(
                ExpressionWrapper(
                    F("price") * F("quantity"), output_field=DecimalField()
                )
            )
        )["total"]

    def get_total_usd(self):
        return self.details.aggregate(
            total=Sum(
                ExpressionWrapper(
                    F("price") / F("order__usd_exchange_value") * F("quantity"),
                    output_field=DecimalField(),
                )
            )
        )["total"]

    @transaction.atomic
    def delete(self, using=None, keep_parents=False):
        # Delete using the class manager method instead of the related one to preserve the stock
        OrderDetail.objects.filter(order_id=self.id).delete()

        super(Order, self).delete(using, keep_parents)


class OrderDetail(models.Model):
    order = models.ForeignKey(
        "sales.Order",
        on_delete=models.CASCADE,
        verbose_name=_("order"),
        related_name="details",
    )
    product = models.ForeignKey(
        "catalog.Product", on_delete=models.PROTECT, verbose_name=_("product")
    )
    quantity = models.PositiveIntegerField(verbose_name=_("quantity"))
    price = models.DecimalField(
        verbose_name=_("price"),
        max_digits=10,
        decimal_places=2,
    )

    objects = OrderDetailQuerySet.as_manager()

    class Meta:
        verbose_name = _("Order line")
        verbose_name_plural = _("Order lines")
        unique_together = ["order", "product"]

    def __str__(self):
        return f"{self.__class__.__name__} {self.id}"

    @classmethod
    def from_db(cls, db, field_names, values):
        """Adds a '_original_values' attribute to the instances readed from DB,
        which has all the original values loaded from the database
        """
        instance = super().from_db(db, field_names, values)

        instance._original_values = dict(zip(field_names, values))

        return instance

    def validate_quantity(self, product: Product = None) -> (bool, Product):
        """Check if the product has enough stock to cover the new values
        and return the product with the new stock and if it should be saved.

        This method DOES NOT save the product, so the caller is responsible
        of call the save method in a transaction.atomic context.

        Args:
            product (Product, optional): if you already have in-memory the product
                with the latest stock, you can pass it as argument the function
                doesn't need to query again. Useful for bulk actions.

        Raises:
            ValueError: the product passed as argument is different than the product in
                the OrderDetail.
            ValidationError: the product has not enough stock for the OrderDetail.

        Returns:
            should_save_product (bool): boolean that indicates that the product stock
                has changed and it should be saved
            product (bool): the product with the new stock
        """
        units_difference = (
            self.quantity - self._original_values["quantity"]
            if self.id
            else self.quantity
        )

        if product is None:
            product = Product.objects.get(id=self.product_id)
        elif product.id != self.product_id:
            raise ValueError(
                "The product passed as argument is different that the one in the OrderDetail."
            )

        if units_difference:
            if units_difference > self.product.stock:
                message = _(
                    "The amount of {name} exceeds the current stock.\nAvailable: {stock}\nAlready reserved: {quantity}"
                ).format(name=product.name, stock=product.stock, quantity=self.quantity)
                raise ValidationError(message)
            elif units_difference:
                product.stock -= units_difference
                self.product = product

        should_save_product = units_difference != 0
        return should_save_product, product

    @transaction.atomic
    def delete(self, *args, **kwargs):
        Product.objects.filter(id=self.product_id).update(
            stock=F("stock") + self.quantity
        )
        super().delete(*args, **kwargs)

    @transaction.atomic
    def save(self, *args, **kwargs):
        product = Product.objects.get(id=self.product_id)
        # TODO change of product
        if not hasattr(self, "_original_values") or (
            hasattr(self, "_original_values")
            and self._original_values["quantity"] != self.quantity
        ):
            should_save_product, product = self.validate_quantity(product)
            if should_save_product:
                product.save()

        if self.price is None:
            self.price = product.price
        super().save(*args, **kwargs)
