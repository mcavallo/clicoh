from rest_framework import serializers

from sales.models import Order, OrderDetail


class OrderSerializer(serializers.ModelSerializer):
    total_units = serializers.IntegerField(read_only=True)
    total_products = serializers.IntegerField(read_only=True)
    total_price = serializers.DecimalField(
        max_digits=10, decimal_places=2, read_only=True
    )
    total_price_usd = serializers.DecimalField(
        max_digits=10, decimal_places=2, read_only=True
    )

    class Meta:
        model = Order
        fields = [
            "id",
            "created",
            "modified",
            "usd_exchange_value",
            "total_products",
            "total_units",
            "total_price",
            "total_price_usd",
        ]


class OrderDetailSerializer(serializers.ModelSerializer):
    order_id = serializers.IntegerField(required=True, read_only=False)
    product_id = serializers.CharField(max_length=15, required=True, read_only=False)
    price = serializers.DecimalField(max_digits=10, decimal_places=2, required=False)

    class Meta:
        model = OrderDetail
        fields = ["id", "order_id", "product_id", "quantity", "price"]


class OrderWithDetailSerializer(OrderSerializer):
    """Order serializer that also have the detail of the order."""

    details = OrderDetailSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = [
            "id",
            "created",
            "modified",
            "usd_exchange_value",
            "total_products",
            "total_units",
            "total_price",
            "total_price_usd",
            "details",
        ]
