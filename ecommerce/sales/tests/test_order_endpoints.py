from decimal import Decimal
from unittest.mock import patch

import pytest

from http import HTTPStatus
from django.urls import reverse

from catalog.models import Product
from sales.models import Order, OrderDetail
from sales.tests.factories import OrderFactory, OrderDetailFactory


@pytest.mark.django_db(transaction=True)
def test_order_list(authenticated_api_client):
    orders = OrderFactory.create_batch(3)
    orders_ids = [order.id for order in orders]
    response = authenticated_api_client.get(reverse("sales:order-list"))

    response_orders = response.json()

    assert response.status_code == HTTPStatus.OK
    assert len(response_orders) == len(orders_ids)
    for order in response_orders:
        assert order["id"] in orders_ids


@pytest.mark.django_db(transaction=True)
def test_order_create(authenticated_api_client):
    with patch("ecommerce.utils.get_exchange_rate", return_value=Decimal("0")):
        response = authenticated_api_client.post(reverse("sales:order-list"), {})

    assert response.status_code == HTTPStatus.CREATED


@pytest.mark.django_db(transaction=True)
def test_order_update(authenticated_api_client):
    original_order = OrderFactory.create()
    new_exchange_rate = original_order.usd_exchange_value - 1

    response = authenticated_api_client.patch(
        reverse("sales:order-detail", kwargs={"pk": original_order.id}),
        format="json",
        data={"usd_exchange_value": new_exchange_rate},
    )

    order = Order.objects.first()

    assert response.status_code == HTTPStatus.OK
    assert order.id == original_order.id
    assert order.usd_exchange_value == new_exchange_rate


@pytest.mark.django_db(transaction=True)
def test_order_delete(authenticated_api_client):
    order = OrderFactory.create()
    order_details = OrderDetailFactory.create_batch(3, order=order)
    expected_product_stock = {
        order_detail.product.id: order_detail.product.stock + order_detail.quantity
        for order_detail in order_details
    }
    response = authenticated_api_client.delete(
        reverse("sales:order-detail", kwargs={"pk": order.id})
    )

    assert response.status_code == HTTPStatus.NO_CONTENT
    assert not Order.objects.count()
    assert not OrderDetail.objects.count()
    for product in Product.objects.all():
        assert product.stock == expected_product_stock[product.id]
