import factory

from catalog.tests.factories import ProductFactory
from sales.models import Order, OrderDetail


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    usd_exchange_value = factory.faker.Faker("pydecimal", left_digits=3, right_digits=2)


class OrderDetailFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OrderDetail

    order = factory.SubFactory(OrderFactory)
    # Ensure stock
    product = factory.SubFactory(
        ProductFactory, stock=factory.faker.Faker("pyint", min_value=100)
    )
    quantity = factory.faker.Faker("pyint", min_value=1, max_value=100)
