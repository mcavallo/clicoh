from decimal import Decimal

import pytest

from http import HTTPStatus
from django.urls import reverse

from catalog.models import Product
from catalog.tests.factories import ProductFactory
from sales.models import Order, OrderDetail
from sales.tests.factories import OrderFactory, OrderDetailFactory


@pytest.mark.django_db(transaction=True)
def test_order_detail_list(authenticated_api_client):
    orders_detail = OrderDetailFactory.create_batch(3)
    orders_detail_ids = [order.id for order in orders_detail]
    response = authenticated_api_client.get(reverse("sales:orderdetail-list"))

    response_orders_detail = response.json()

    assert response.status_code == HTTPStatus.OK
    assert len(response_orders_detail) == len(orders_detail_ids)
    for order in response_orders_detail:
        assert order["id"] in orders_detail_ids


@pytest.mark.django_db(transaction=True)
def test_order_detail_create(authenticated_api_client):
    product = ProductFactory.create(stock=300)
    order_detail_data = {
        "product_id": product.id,
        "order_id": OrderFactory.create().id,
        "quantity": 100,
    }
    response = authenticated_api_client.post(
        reverse("sales:orderdetail-list"), order_detail_data
    )

    created_order_detail = response.json()

    updated_product = Product.objects.get(id=product.id)

    assert response.status_code == HTTPStatus.CREATED
    assert Decimal(created_order_detail["price"]) == product.price
    assert updated_product.stock == (product.stock - order_detail_data["quantity"])


@pytest.mark.django_db(transaction=True)
def test_order_detail_update(authenticated_api_client):
    order_detail = OrderDetailFactory.create(quantity=10)
    product_stock = order_detail.product.stock
    order_detail_data = {
        "product_id": order_detail.product_id,
        "order_id": order_detail.order_id,
        "quantity": 15,
        "price": order_detail.product.price + 1,
    }

    response = authenticated_api_client.patch(
        reverse("sales:orderdetail-detail", kwargs={"pk": order_detail.id}),
        order_detail_data,
    )

    updated_order_detail = response.json()

    updated_product = Product.objects.get(id=order_detail.product_id)

    assert response.status_code == HTTPStatus.OK
    assert Decimal(updated_order_detail["price"]) == order_detail_data["price"]
    assert updated_product.stock == (
        product_stock - updated_order_detail["quantity"] + order_detail.quantity
    )


@pytest.mark.django_db(transaction=True)
def test_order_detail_delete(authenticated_api_client):
    order = OrderFactory.create()
    order_details = OrderDetailFactory.create_batch(3, order=order)
    order_to_delete = order_details[0]
    expected_product_id = order_to_delete.product.id
    expected_product_stock = order_to_delete.product.stock + order_to_delete.quantity

    response = authenticated_api_client.delete(
        reverse("sales:orderdetail-detail", kwargs={"pk": order_details[0].id})
    )

    updated_product = Product.objects.get(id=expected_product_id)

    assert response.status_code == HTTPStatus.NO_CONTENT
    assert Order.objects.count() == 1
    assert OrderDetail.objects.count() == 2
    assert expected_product_stock == updated_product.stock


@pytest.mark.skip
@pytest.mark.django_db(transaction=True)
def test_order_detail_update_product_id(authenticated_api_client):
    order_detail = OrderDetailFactory.create(quantity=10)
    new_product = ProductFactory.create()
    old_product_expected_stock = order_detail.product.stock + order_detail.quantity
    order_detail_data = {
        "product_id": new_product.id,
        "order_id": order_detail.order_id,
        "quantity": order_detail.quantity,
    }

    response = authenticated_api_client.patch(
        reverse("sales:orderdetail-detail", kwargs={"pk": order_detail.id}),
        order_detail_data,
    )

    updated_order_detail = response.json()

    old_product = Product.objects.get(id=order_detail.product_id)
    new_product_original_stock = new_product.stock
    new_product.refresh_from_db()

    assert response.status_code == HTTPStatus.OK
    assert updated_order_detail["product_id"] == new_product.id
    assert old_product.stock == old_product_expected_stock
    assert new_product.stock == (new_product_original_stock - order_detail.quantity)
