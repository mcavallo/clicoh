from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated

from sales.models import Order, OrderDetail
from sales.serializers import (
    OrderSerializer,
    OrderWithDetailSerializer,
    OrderDetailSerializer,
)


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.annotate_totals()
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ["id"]
    ordering_fields = ["id", "created", "modified"]
    ordering = ["id"]

    def get_serializer_class(self):
        """Use the OrderWithDetail serializer for the retrieve action, otherwise OrderSerializer"""
        if hasattr(self, "action"):
            if self.action == "retrieve":
                return OrderWithDetailSerializer
            else:
                return OrderSerializer


class OrderDetailViewSet(viewsets.ModelViewSet):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ["id", "product_id", "order_id"]
    ordering_fields = ["id", "product_id", "order_id"]
    ordering = ["order_id", "id"]
