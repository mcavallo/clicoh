from rest_framework import routers

from sales.viewsets import OrderViewSet, OrderDetailViewSet

router = routers.DefaultRouter()
router.register("order", OrderViewSet)
router.register("order-detail", OrderDetailViewSet)
