import os

import dj_database_url

from .base import *  # noqa

DEBUG = False


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

db_from_env = dj_database_url.config(conn_max_age=600)
DATABASES = {"default": db_from_env}

# Heroku url
ALLOWED_HOSTS = ["clicoh-mcavallo.herokuapp.com"]


STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
