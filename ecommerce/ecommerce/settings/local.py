from .base import *  # noqa

DEBUG = True

INSTALLED_APPS += ["debug_toolbar"]

# django toolbar
INTERNAL_IPS = ["localhost", "127.0.0.1"]


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "ecommerce",
        "USER": "clicoh",
        "PASSWORD": "clicoh",
        "HOST": "localhost",
        "PORT": 5432,
    }
}

MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]

CORS_ORIGIN_WHITELIST = [
    "http://localhost:3000",
]
