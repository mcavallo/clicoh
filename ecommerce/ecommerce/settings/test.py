import os

import dj_database_url

from .base import *  # noqa


DEBUG = False

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "ecommerce",
        "USER": "clicoh",
        "PASSWORD": "clicoh",
        "HOST": "localhost",
        "PORT": 5432,
        "TEST": {
            "NAME": os.environ.get("TEST_DATABASE", "ecommerce_test"),
        },
    }
}

# Heroku: Update database configuration from $DATABASE_URL.
db_from_env = dj_database_url.config(conn_max_age=600)
DATABASES["default"].update(db_from_env)
