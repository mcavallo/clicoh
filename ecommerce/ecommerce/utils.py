import logging
from decimal import Decimal

import requests
from requests.exceptions import HTTPError, ConnectionError
from django.core.cache import cache

CACHE_KEY = "exchange_rate"


def get_exchange_rate() -> Decimal:
    """Get the dollar blue value from the DolarSi API.

    The value is saved to the cache to save time when creating orders.
    May have a delay up to 60 seconds.

    Returns:
        exchange_rate (Decimal): the current dollar exchange rate,
            or Decimal(0) if the dolarsi service is unavailable.
    """
    exchange_rate = cache.get(CACHE_KEY)
    if not exchange_rate:
        try:
            response = requests.get(
                "https://www.dolarsi.com/api/api.php?type=valoresprincipales"
            )
            response.raise_for_status()

            exchange_rates = response.json()
            blue_usd_exchange_rate = list(
                filter(lambda x: x["casa"]["nombre"] == "Dolar Blue", exchange_rates)
            )[0]
            exchange_rate = blue_usd_exchange_rate["casa"]["venta"]
            exchange_rate = Decimal(exchange_rate.replace(",", "."))

        except (HTTPError, ConnectionError, KeyError):
            logging.warning(
                "Cannot retrieve the value of the blue dollar from DolarSi."
            )
            exchange_rate = Decimal("0")

        cache.set(CACHE_KEY, exchange_rate, timeout=60)
    return exchange_rate
