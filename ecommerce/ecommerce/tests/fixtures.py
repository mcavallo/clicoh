from decimal import Decimal
from http import HTTPStatus
from unittest.mock import MagicMock, Mock

from requests import HTTPError

dolarsi_response_ok = MagicMock()
dolarsi_response_ok.status_code = HTTPStatus.OK
dolarsi_response_ok.json = lambda: [
    {
        "casa": {
            "compra": "94,710",
            "venta": "100,710",
            "agencia": "349",
            "nombre": "Dolar Oficial",
            "variacion": "0",
            "ventaCero": "TRUE",
            "decimales": "3",
        }
    },
    {
        "casa": {
            "compra": "159,000",
            "venta": "164,000",
            "agencia": "310",
            "nombre": "Dolar Blue",
            "variacion": "0,610",
            "ventaCero": "TRUE",
            "decimales": "3",
        }
    },
]


dolarsi_response_error = MagicMock()
dolarsi_response_error.raise_for_status.side_effect = Mock(side_effect=HTTPError())

dolarsi_response_wrong = MagicMock()
dolarsi_response_wrong.status_code = HTTPStatus.OK
dolarsi_response_wrong.json = lambda: [
    {
        "casa": {
            "compra": "94,710",
            "venta": "100,710",
            "agencia": "349",
            "nombre": "Dolar Oficial",
            "variacion": "0",
            "ventaCero": "TRUE",
            "decimales": "3",
        }
    },
]

exchange_rate_fixtures = [
    # OK response
    (Decimal("164"), dolarsi_response_ok),
    # Error response
    (Decimal("0"), dolarsi_response_error),
    # Wrong json response
    (Decimal("0"), dolarsi_response_error),
]
