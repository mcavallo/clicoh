import pytest

from unittest.mock import patch

from ecommerce.utils import get_exchange_rate
from ecommerce.tests.fixtures import exchange_rate_fixtures


@pytest.mark.parametrize(
    "expected_exchange_rate, dolarsi_response", exchange_rate_fixtures
)
def test_get_exchange_rate(expected_exchange_rate, dolarsi_response):
    with patch("ecommerce.utils.requests.get", return_value=dolarsi_response), patch(
        "ecommerce.utils.cache.get", return_value=None
    ):
        exchange_rate = get_exchange_rate()

    assert exchange_rate == expected_exchange_rate
