from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from catalog.models import Product


class StockFilter(admin.SimpleListFilter):
    """Filter the products by the ones with/without stock."""

    title = _("stock")

    parameter_name = "has_stock"

    def lookups(self, request, model_admin):
        return (1, _("With stock")), (0, _("Without stock"))

    def queryset(self, request, queryset):
        has_stock = self.value()

        if has_stock is not None:
            has_stock = bool(int(has_stock))
            if has_stock:
                queryset = queryset.filter(stock__gt=0)
            else:
                queryset = queryset.filter(stock=0)

        return queryset


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "stock", "price"]
    list_filter = [StockFilter]
    search_fields = ["id", "name"]
