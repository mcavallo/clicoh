from rest_framework import routers

from catalog.viewsets import ProductViewSet

router = routers.DefaultRouter()
router.register("product", ProductViewSet)
