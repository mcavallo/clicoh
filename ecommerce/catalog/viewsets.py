from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated

from catalog.models import Product
from catalog.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ["id", "name"]
    ordering_fields = ["id", "name", "price", "stock"]
    ordering = ["id"]
