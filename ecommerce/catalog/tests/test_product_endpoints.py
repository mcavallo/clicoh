from decimal import Decimal

import pytest

from http import HTTPStatus
from django.urls import reverse

from catalog.models import Product
from catalog.tests.factories import ProductFactory


@pytest.mark.django_db(transaction=True)
def test_product_list(authenticated_api_client):
    products = ProductFactory.create_batch(3)
    product_ids = [product.id for product in products]
    response = authenticated_api_client.get(reverse("catalog:product-list"))

    response_products = response.json()

    assert response.status_code == HTTPStatus.OK
    assert len(response_products) == len(product_ids)
    for product in response_products:
        assert product["id"] in product_ids


@pytest.mark.django_db(transaction=True)
def test_product_create(authenticated_api_client):
    product_data = ProductFactory.build().__dict__
    product_data.pop("_state")
    response = authenticated_api_client.post(
        reverse("catalog:product-list"), product_data
    )

    assert response.status_code == HTTPStatus.CREATED


@pytest.mark.django_db(transaction=True)
@pytest.mark.parametrize(
    ["product_data"],
    [
        [{"id": "foo", "name": "bar", "stock": -3}],  # Negative stock
        [{"name": "bar"}],  # No id
        [{"id": "foo"}],  # No name
    ],
)
def test_product_cannot_create(product_data: dict, authenticated_api_client):
    response = authenticated_api_client.post(
        reverse("catalog:product-list"), product_data
    )

    assert response.status_code == HTTPStatus.BAD_REQUEST


@pytest.mark.django_db(transaction=True)
def test_product_update(authenticated_api_client):
    original_product = ProductFactory.create()
    new_product_data = ProductFactory.build().__dict__
    new_product_data.pop("_state")
    new_product_data.pop("id")

    response = authenticated_api_client.patch(
        reverse("catalog:product-detail", kwargs={"pk": original_product.id}),
        format="json",
        data=new_product_data,
    )

    product = Product.objects.first()

    assert response.status_code == HTTPStatus.OK
    assert product.id == original_product.id
    assert product.name == new_product_data["name"]
    assert product.price == new_product_data["price"]
    assert product.stock == new_product_data["stock"]


@pytest.mark.django_db(transaction=True)
def test_product_delete(authenticated_api_client):
    product = ProductFactory.create()
    response = authenticated_api_client.delete(
        reverse("catalog:product-detail", kwargs={"pk": product.id})
    )

    assert response.status_code == HTTPStatus.NO_CONTENT
    assert not Product.objects.count()
