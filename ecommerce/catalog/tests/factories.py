import factory

from catalog.models import Product


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    id = factory.faker.Faker("pystr", max_chars=15)
    name = factory.faker.Faker("pystr", max_chars=100)
    price = factory.faker.Faker("pydecimal", left_digits=8, right_digits=2)
    stock = factory.faker.Faker("pyint")
