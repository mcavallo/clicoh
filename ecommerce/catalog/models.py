from decimal import Decimal

from django.db import models
from django.utils.translation import gettext_lazy as _


class Product(models.Model):
    """Represents a product with price and stock for sale."""

    id = models.CharField(primary_key=True, max_length=15)
    name = models.CharField(verbose_name=_("name"), max_length=100)
    price = models.DecimalField(
        verbose_name=_("price"),
        max_digits=10,
        decimal_places=2,
        default=Decimal("0"),
    )
    stock = models.PositiveIntegerField(verbose_name=_("stock"), default=0)

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __str__(self):
        return f"({self.id}) {self.name} - stock: {self.stock}"
