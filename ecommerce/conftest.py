import pytest
from django.contrib.auth.models import User
from rest_framework.test import APIClient


@pytest.fixture
def authenticated_api_client():
    user = User.objects.create(username="user")
    api_client = APIClient()
    api_client.force_authenticate(user)
    return api_client
