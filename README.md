# Ecommerce backend project

**[Requirements](project_requirements.pdf)**

#### Demo app

Swagger: https://clicoh-mcavallo.herokuapp.com/  
Django Admin: https://clicoh-mcavallo.herokuapp.com/admin  

###### Credentials 
Username: admin  
Password: clicoh2021

---
# Installation & usage
###### Compatible and tested for Python 3.8
## Local execution
```Bash
# Start the database and run the server
docker-compose up web

# Browse on http://localhost:8000 or http://127.0.0.1:8000
```

## Test
```Bash
# Start the database
docker-compose up -d db

# Install the test requirements 
pip install -r requirements-test.txt

# Run tests with coverage
cd ecommerce
coverage run -m pytest

# Generate and open the coverage report
coverage html
python -m webbrowser ./htmlcov/index.html
```
If you want to exclude a file from being coverage tested, you can do that in the file `ecommerce/.coveragerc`

## Develop
```Bash
# Start the database
docker-compose up -d db

# Install the test requirements 
pip install -r requirements-dev.txt

cd ecommerce/

# set the default configuration to local
export DJANGO_SETTINGS_MODULE=ecommerce.settings.local

# Migrate
./manage.py migrate

# Create a super user account
./manage.py createsuperuser
# Follow the instructions

# Execute the server
./manage.py runserver
# Browse with localhost:8000

# You can also open a python interactive shell with
./manage.py shell_plus

# Navigate the database with
./manage.py db_shell

# Format the code with
black .
```

---
## Class Diagram
![class diagram](class_diagram.png)

---
## Assumptions/decisions taken through the project for features without enough definition
### Products
* The price can be negative to allow discounts.
* The code can't go longer than 15 chars, and the name 100.
* I used the PK as string because was requested, but it's not recommended since DRF urls can't handle some characters

### Orders
* The order are sale orders (not delivery or purchase)
* The unit price of the order line shouldn't change once sold if the price of the product changes
* If the dollar price cannot be retrieved from DolarSi, the default value is 0.00 and logs a warning
* Creation/deletion of orders is made with atomic transaction because involves risky modifications 
  to other model (the stock in the products), that without it the user may leave the database in an
  inconsistent status.
* If the user attempts to save twice the same product for a sale order, prevent the save of the order
  (doesn't merge the units).
* Even if I created the `get_total` and `get_total_usd` methods, I didn't use them because this generates extra queries, 
  so I added a custom queryset method to annotate those values.

### Dollar Price
The dollar price is saved in cache for 1 minute to speed up orders creation if many are made at the same time, without
compromising too much the use of an old value.

The cache is implemented with the django default one, so it isn't shareable between deployed instances.
This means that if two instances are deployed, the blue dollar value may be de-synced.

The use of `memcached` or `redis` is preferred, but for this POC with one instance is enough.
Also, a celery beat is an option to make the cache update.

### CI/CD
I used gitlab as development platform because I really wanted to try out the CI/CD feature.
Was my first time using it, so for sure there are a lot of things to improve.  
#### Working features:
  * Run the tests each time that a commit is pushed to a branch
  * The tests generate a report that is saved and can be downloaded from gitlab
  * If the tests fail, prevents the merges
  * Performs an automatic deploy to heroku when pushed branch is `main` (if the tests passed)
  * The database for the tests and "production" are both hosted on heroku

### Docker
I added a Dockerfile/docker-compose to run the project in a local environment.
If you want to try it out local, you don't even need to install Python, install the requirements, create the db, etc.

### Database
Postgres is the chosen one for cloud and local.
It is initialized with docker, so you don't have to install and setup it by yourself.
###### Credentials
**host:** localhost  
**db name:** ecommerce  
**user:** clicoh  
**password:** clicoh  

### Quality
* **[Coverage](https://pypi.org/project/coverage/)** combined with pytest, which shows the lines of the code are not covered by tests.
* **[Black](https://github.com/psf/black)**, a code formatter that respects pep8 and doesn't compromise the functionalities.

### Debugging
* **[Django debug toolbar](https://github.com/jazzband/django-debug-toolbar)** in local-mode to validate that the
  queries on the django views are optimal.
* **[Django silk](https://github.com/jazzband/django-silk)** same as Django debug toolbar, but for DRF.

### Documentation
* **[DRF-YASG](https://pypi.org/project/drf-yasg/)**, an automatic documentation generator for DRF, which let you test the APIs from the browser.
* **Docstrings** following the [Google Style Python Docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) on the classes/functions that are not self-explanatory.
* **Type-hinting** for the function definitions, which are interpreted by most of the IDEs.

### Testing
The ideal is to make unittest that doesn't hit the DB and validate that the save/delete/etc methods are called with certain values,
but takes a bit more of time to code, so I wrote the tests using factories/endpoint calls that write the db.

There is one case that skipped in the tests because it fails, and I had no time to fix it.
The case is when you update an OrderDetail by modifying the product_id.
That case doesn't return the stock of the original product, nor discount it from the new product.
I should make a whole new function to cover that case, which I can make if you want.


### Others
* Django admin with custom views for the products and orders.
* **[Django extensions](https://pypi.org/project/django-extensions/) + [iPython](https://pypi.org/project/ipython/)**. With `./manage.py shell_plus` you can start an interactive shell with all the models imported.
  Also have other good features like abstract models like `TitleSlugDescriptionModel` or `TimestampedModel` which save time and lines of code.
* Translations in spanish
