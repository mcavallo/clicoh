# syntax=docker/dockerfile:1
FROM python:3.8
ENV PYTHONUNBUFFERED=1
ENV DJANGO_SETTINGS_MODULE=ecommerce.settings.local
WORKDIR /code
COPY requirements.txt /code/
COPY requirements-test.txt /code/
COPY requirements-dev.txt /code/
RUN pip install -r requirements-dev.txt
COPY ./ecommerce/ /code/