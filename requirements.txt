django==3.*
django-extensions==3.*
djangorestframework==3.*
djangorestframework-simplejwt==4.*
django-cors-headers==3.*
drf-yasg==1.*
django-filter==2.*
requests==2.*
dj-database-url==0.*
psycopg2-binary==2.*
django-environ==0.*

gunicorn==20.*
whitenoise==5.*